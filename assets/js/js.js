// var key= "24d3b8c688ddba9b2aa4346c58bac156";
var key= "00865a67c5e1b2d7afcacd232bfad4f2";
var procesoCity = {
    init: function () {
      this.dep();
      this.limp();
    },
  
    dep: function () {
      var url = "http://battuta.medunes.net/api"
      $.ajax({
        url: url+"/country/all/?callback=?&key="+key,
        type: "GET",
        crossDomain: true,
        dataType: "json",
        success: function (res) {
                var obj = res;

                var contenido = '<option value="">Seleccione un pais</option>';
                
                for (var raw of obj) {
                contenido += '<option value="' + raw.code + '">' + raw.name + '</option>';
                }

                $("#pais").html(contenido);
                $('#region').prop('disabled', true);
                $('#city').prop('disabled', true);
                region();
                limp();
        },
      });
    },
  
    limp: function () {
      $("#lim_sel_").click(function () {
        limp();
        $("#pais").text('');
        $("#region").text('');
        $("#city").text('');
        $(".off_").hide();
        $(".offf_").hide();
        dep();
      });
    },
  
};
  
  function limp() {
    var city = $("#city").val();
    if(city!=null) {
        if(city!='') {
            $("#lim_sel_").show();
        }
    }else{
        $("#lim_sel_").hide();
    }
  }
  
  function dep() {
    var url = "http://battuta.medunes.net/api"
      $.ajax({
        url: url+"/country/all/?callback=?&key="+key,
        type: "GET",
        crossDomain: true,
        dataType: "json",
        success: function (res) {
                var obj = res;

                var contenido = '<option value="">Seleccione un pais</option>';
                
                for (var raw of obj) {
                contenido += '<option value="' + raw.code + '">' + raw.name + '</option>';
                }

                $("#pais").html(contenido);
                $('#region').prop('disabled', true);
                $('#city').prop('disabled', true);
                region();
                limp();
        },
      });
  }
  
  function region() {
    $("#pais").change(function () {
      var region = $("#pais").val();
      if(region!='') {
        $(".off_").show();
        $('#region').prop('disabled', false);
        var url = "http://battuta.medunes.net/api"
        $.ajax({
          url: url+"/region/"+region+"/all/?callback=?&key="+key,
          type: "GET",
          crossDomain: true,
          dataType: "json",
          success: function (res) {
            var obj = res;
            var contador = 1;
            var contenido = '<option value="">Seleccione una region o departamento</option>';
            for (var raw of obj) {
              contenido += '<option value="' + raw.region + '">' + raw.region + '</option>';
              contador++;
            }
            $("#region").html(contenido);
            city();
            limp();
          },
        });
      }else{
        $(".off_").hide();
        $(".offf_").hide();
        $("#region").text('');
        $("#city").text('');
        $('#region').prop('disabled', true);
        $('#city').prop('disabled', true);
      }
    });
  }

  function city() {
    $("#region").change(function () {
      var region = $("#region").val();
      if(region!='') {
        $(".offf_").show();
        $('#city').prop('disabled', false);
        var pais = $("#pais").val();
        var url = "http://battuta.medunes.net/api"
          $.ajax({
              url: url+"/city/"+pais+"/search/?region="+region+"&callback=?&key="+key,
              type: "GET",
              crossDomain: true,
              dataType: "json",
              success: function (res) {
              var obj = res;
              var contador = 1;
              var contenido = '<option value="">Seleccione una ciudad</option>';
              for (var raw of obj) {
                  contenido += '<option value="' + raw.city + '">' + raw.city + '</option>';
                  contador++;
              }
              $("#city").html(contenido);
              limp();
              conf();
          },
        });
      }else{
        $(".offf_").hide();
        $("#city").text('');
        $('#city').prop('disabled', true);
      }
    });
  }

  function conf() {
    $("#city").change(function () {
        limp();
    });
  }

$(document).ready(function() {
    procesoCity.init();
});
  