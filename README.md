# form

Formulario de paises y ciudades

## Pasos a seguir:

1. Ingresar al repositorio https://gitlab.com/Davin12v/form y clona el archivo en zip. 

2. Importa el zip en los plugins de WordPress, instalarlo y activarlo.

3. En mi caso cree una plantilla en blanco e integre el "Shortcode" del plugin que para este caso es: [form]. 

4. Guarde los cambios realizados y previsualice la página creada con el plugin del formulario.

Repositorio: https://gitlab.com/Davin12v/form