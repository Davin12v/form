<?php
/**
 * Plugin Name: Form countries and cities
 * Plugin URI: 
 * Description: Inserta un furmulario con paises y ciudades.
 * Version: 1.0.0
 * Author: Davison
 * Author URI: 
 * License: GPL2
 */
add_shortcode( "form", function(){
	$output = '<div class="container">
                <form class="form_style row">
                    <div class="col-md-12">
                        <img src="https://demosadmin.tech/public/air.png" class="img_style">
                    </div>

                    <h4 class="h4_style">Vuelos disponibles</h4>

                    <div class="col-md-12">
                        <label class="label_style label_top">País</label>

                        <div class="btn_close_div" id="lim_sel_">
                            <button type="button" class="btn_close_btn">
                                <span aria-hidden="true">x</span>
                            </button>
                        </div>

                        <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" id="pais">
                        </select>
                    </div>

                    <div class="col-md-12">
                        <label class="label_style off_ label_top">Región</label>
                        <select class="form-select form-select-lg mb-3 off_" aria-label=".form-select-lg example" id="region">
                        </select>
                    </div>

                    <div class="col-md-12">
                        <label class="label_style offf_ label_top">Ciudad</label>
                        <select class="form-select form-select-lg mb-3 offf_" aria-label=".form-select-lg example" id="city">
                        </select>
                    </div>
                    
                    <div class="col-md-6">
                        <label class="label_style label_top">Fecha</label>
                        <input type="date" class="form-control" />
                    </div>
                    <div class="col-md-6">
                        <label class="label_style label_top">Hora</label>
                        <input type="time" class="form-control" />
                    </div>
                    

                    <div class="btn_searh">
                        <button type="button" class="btn btn-info form-control">Buscar</button>
                    </div>
                </form>
                </div>';
	return $output;
});

// Include bootstrap, jquery, js and css
function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', "https://code.jquery.com/jquery-3.5.1.min.js", array(), '3.5.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

function GetbootstrapCSS(){
    wp_enqueue_style('GetbootstrapCSS',plugins_url('assets/bootstrap/css/bootstrap.min.css',__FILE__));
}
add_action('wp_enqueue_scripts','GetbootstrapCSS');

function css(){
    wp_enqueue_style('css',plugins_url('assets/css/style.css',__FILE__));
}
add_action('wp_enqueue_scripts','css');

function GetbootstrapJS(){
    wp_enqueue_script('GetbootstrapJS',plugins_url('assets/bootstrap/js/bootstrap.min.js',__FILE__),array('jquery'),'3.5.1', true);
}
add_action('wp_enqueue_scripts','GetbootstrapJS');

function GetJS(){
    wp_enqueue_script('GetJS',plugins_url('assets/js/js.js',__FILE__),array('jquery'),'3.5.1', true);
}
add_action('wp_enqueue_scripts','GetJS');

